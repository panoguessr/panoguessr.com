# PanoGuessr.com

**➡️ Let's play now on https://panoguessr.com**

![Logo PanoGuessr](<Logo PanoGuessr.png>)

"Where is this [Panoramax](https://gitlab.com/panoramax) picture located again?"


## License

This project is licensed under the MIT license. See the [LICENSE](./LICENSE) file for details.

This project has been started by Corentin Barbedette, Clément Guibout and Maxandre Rocherfort, under the supervision of Adrien Pavie.

## Credits

- Background music:

Original music made from https://suno.com.


- NPM dependencies:

```bash
├─ @nuxt/icon
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/nuxt/icon
├─ @nuxtjs/leaflet
│  ├─ licenses: Apache-2.0
│  ├─ repository: https://github.com/nuxt-modules/leaflet
├─ @panoramax/web-viewer
│  ├─ licenses: MIT
│  ├─ repository: git+https://gitlab.com/panoramax/clients/web-viewer
│  ├─ publisher: Panoramax team
├─ @pinia/nuxt
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/vuejs/pinia
│  ├─ publisher: Eduardo San Martin Morote
│  ├─ email: posva13@gmail.com
├─ @vue-leaflet/vue-leaflet
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/vue-leaflet/vue-leaflet
│  ├─ publisher: Nicolò Maria Mezzopera
├─ geojson@0.5.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/caseycesari/geojson.js
│  ├─ publisher: Casey Cesari
├─ howler@2.2.4
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/goldfire/howler.js
│  ├─ publisher: James Simpson
│  ├─ email: james@goldfirestudios.com
│  ├─ url: http://goldfirestudios.com
├─ leaflet@1.9.4
│  ├─ licenses: BSD-2-Clause
│  ├─ repository: https://github.com/Leaflet/Leaflet
├─ nuxt@3.15.4
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/nuxt/nuxt
├─ vue-router@4.4.5
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/vuejs/router
│  ├─ publisher: Eduardo San Martin Morote
│  ├─ email: posva13@gmail.com
└─ vue@3.5.11
   ├─ licenses: MIT
   ├─ repository: https://github.com/vuejs/core
   ├─ publisher: Evan You
```
